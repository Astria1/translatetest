package ru.testing;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.testing.entities.Translate;
import ru.testing.gateway.YandexTranslateGateway;

public class YandexTranslateTest {
    private static final String lang = "en-ru";
    private static final String text = "Hello World";

    @Test
    @DisplayName("Переводчик")
    public void getTranslate(){
        YandexTranslateGateway yandexTranslate = new YandexTranslateGateway();

        Translate testTranslate  = yandexTranslate.getTranslate(lang, text);
        Assertions.assertEquals(testTranslate.getLang(), lang);
        Assertions.assertEquals(testTranslate.getText().get(0).toString(), "Привет, Мир!");
    }
}