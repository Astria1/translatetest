package ru.testing.gateway;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import ru.testing.entities.Translate;

@Slf4j
public class YandexTranslateGateway {
    private static final String URL = "https://translate.yandex.net/api/v1.5/tr.json/translate";
    private static final String TOKEN = "";

    @SneakyThrows
    public Translate getTranslate(String language, String text) {
        Gson gson = new Gson();
        HttpResponse<String> response = Unirest.get(URL)
                .header("Accept", "*/*")
                .queryString("key", TOKEN)
                .queryString("leng", language)
                .queryString("text", text)
                .asString();

        String strResponse = response.getBody();
        log.info("response: "+strResponse);
        return gson.fromJson(strResponse, Translate.class);
    }
}