package ru.testing.entities;

import com.google.gson.JsonArray;
import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Translate {
    @SerializedName("code")
    public Integer code;
    @SerializedName("lang")
    public String lang;
    @SerializedName("text")
    public JsonArray text;
}